Feature: update a bin json
    
    @smokeTest
    Scenario: user fires the request to update a specific Bin using with correct information, the response should have 200 status code
        When the user updates a private bin
        Then the response status should be "200" for "validPutRequest"
    
    @smokeTest
    Scenario: user updates a bin should response contain updated property
        When the user updates a private bin
        Then the response should contain the property updated

    
    @regressionTest
    Scenario Outline: user fires the request to update a private Bin with incorrect information, the response should have the expected error message - "<condition>"
        When the user updates a private bin with "<condition>"
        Then the response should contain the expected "<condition>"
        Examples:
            | condition          |
            | invalidBinId       |
            | binNotFound        |
            | invalidContentType |
            | blankBin           |
            | emptyHeader        |
    
    @regressionTest
    Scenario Outline: user fires the request to update a private Bin with incorrect information, the response should have the right status code - "<condition>"
        When the user updates a private bin with "<condition>"
        Then the response status should be "<responseCode>" for "invalidPutRequest"
        Examples:
            | condition          | responseCode |
            | invalidBinId       | 400          |
            | invalidContentType | 400          |
            | blankBin           | 400          |
            | emptyHeader        | 401          |
            | binNotFound        | 404          |
