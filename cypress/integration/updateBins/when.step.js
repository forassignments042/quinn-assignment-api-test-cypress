import { When } from "cypress-cucumber-preprocessor/steps";
("use strict");
const updateService = require("../../serviceObjectModel/PUT");
let putReq = new updateService();
(function () {
  When("the user updates a private bin", () => {
    putReq.update_JsonBin();
  });
  When("the user updates a private bin with {string}", (condtion) => {
    expect(condtion).not.to.be.empty;
    putReq.sendReqWithCond(condtion);
  });
})();
