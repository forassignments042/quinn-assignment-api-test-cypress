import { When } from "cypress-cucumber-preprocessor/steps";
("use strict");
const getService = require("../../serviceObjectModel/GET");
let getReq = new getService();
(function () {
  When("the user requests a private bin", () => {
    getReq.readPrivateBin();
  });
  When("the user requests a private bin with {string}", (condtion) => {
    expect(condtion).not.to.be.empty;
    getReq.sendReqWithCond(condtion);
  });
})();
