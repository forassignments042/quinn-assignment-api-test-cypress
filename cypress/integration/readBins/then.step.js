import { Then } from "cypress-cucumber-preprocessor/steps";
("use strict");
const getService = require("../../serviceObjectModel/GET");
let getReq = new getService();
(function () {
  Then("the response should contain the expected {string}", (error) => {
    expect(error).not.to.be.empty;
    getReq.shouldErrorMessageBe(error);
  });
  Then("the response should contain the expected body", () => {
    getReq.expectedPrivateBinBodyShouldBeReturned("validGetRequest");
  });
})();
