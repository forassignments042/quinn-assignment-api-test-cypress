import { When } from "cypress-cucumber-preprocessor/steps";
("use strict");
const createService = require("../../serviceObjectModel/POST");
let crReq = new createService();
(function () {
  When("the user creates a private bin", () => {
    crReq.add_JsonBin();
  });
  When("the user creates a private bin with {string}", (condtion) => {
    expect(condtion).not.to.be.empty;
    crReq.sendReqWithCond(condtion);
  });
})();
