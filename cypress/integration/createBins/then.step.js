import { Then } from "cypress-cucumber-preprocessor/steps";
("use strict");
const createService = require("../../serviceObjectModel/POST");
let crReq = new createService();
(function () {
  Then("the response should contain the property created", () => {
    crReq.doesCreatedBinContainsExpectedRecord();
  });
  Then("the response should contain the expected {string}",(error) => {
    expect(error).not.to.be.empty;
    crReq.shouldErrorMessageBe(error);
  });
})();
