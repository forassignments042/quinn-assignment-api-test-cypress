Feature: retrieving a bin json
    
    @smokeTest
    Scenario: user fires the request to read a specific Bin using with correct information, the response should have 200 status code
        When the user requests a private bin
        Then the response status should be "200" for "validGetRequest"
    
    @smokeTest
    Scenario: user fires the request to read a specific bin using with correct information, the response should contain the expected body
        When the user requests a private bin
        Then the response should contain the expected body
    
    @regressionTest
    Scenario Outline: user fires the request to read a private Bin with incorrect information, the response should have the expected error message - "<condition>"
        When the user requests a private bin with "<condition>"
        Then the response should contain the expected "<condition>"
        Examples:
            | condition     |
            | invalidBinId  |
            | binNotFound   |
            | invalidHeader |
            | emptyHeader   |
    
    @regressionTest
    Scenario Outline: user fires the request to read a private Bin with incorrect information, the response should have the right status code - "<condition>"
        When the user requests a private bin with "<condition>"
        Then the response status should be "<responseCode>" for "invalidGetRequest"
        Examples:
            | condition     | responseCode |
            | invalidBinId  | 400          |
            | binNotFound   | 404          |
            | invalidHeader | 401          |
            | emptyHeader   | 401          |
