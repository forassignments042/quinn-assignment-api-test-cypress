Feature: create a bin json


    @smokeTest
    Scenario: user fires the request to create a specific Bin using with correct information, the response should have 200 status code
        When the user creates a private bin
        Then the response status should be "200" for "validPostRequest"
    
    @smokeTest
    Scenario: user creates a bin should response contain created property
        When the user creates a private bin
        Then the response should contain the property created

    @regressionTest
    Scenario Outline: user fires the request to create a private Bin with incorrect information, the response should have the expected error message - "<condition>"
        When the user creates a private bin with "<condition>"
        Then the response should contain the expected "<condition>"
        Examples:
            | condition |
            | invalidContentType |
            | blankBin           |
            | emptyHeader        |

    @regressionTest
    Scenario Outline: user fires the request to create a private Bin with incorrect information, the response should have the right status code - "<condition>"
        When the user creates a private bin with "<condition>"
        Then the response status should be "<responseCode>" for "invalidPostRequest"
        Examples:
            | condition | responseCode |

            | invalidContentType | 400 |
            | blankBin           | 400 |
            | emptyHeader        | 401 |
