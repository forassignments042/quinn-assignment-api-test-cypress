"use strict";
const getRequest = require("../../serviceObjectModel/GET");
let getReq = new getRequest();
import { Then } from "cypress-cucumber-preprocessor/steps";

(function () {
  Then(
    "the response status should be {string} for {string}",
    (responseCode, request) => {
      expect(responseCode).not.to.be.empty;
      expect(request).not.to.be.empty;
      getReq.shouldResponseStatusCodeBe(request, parseInt(responseCode));
    }
  );
})();
