Feature: delete a bin json
    
    @smokeTest
    Scenario: user fires the request to delete a specific Bin using with correct information, the response should have 200 status code
        When the user deletes a private bin
        Then the response status should be "200" for "validDeleteRequest"
    
    @smokeTest
    Scenario: user fires the request to delete a specific bin using with correct information, the response should contain the expected body
        When the user deletes a second private bin
        Then the response should contain a success message
    
    @regressionTest
    Scenario Outline: user fires the request to delete a private Bin with incorrect information, the response should have the expected error message - "<condition>"
        When the user deletes a private bin with "<condition>"
        Then the response should contain the expected "<condition>"
        Examples:
            | condition    |
            | invalidBinId |
            | emptyHeader  |
    
    @regressionTest
    Scenario Outline: user fires the request to delete a private Bin with incorrect information, the response should have the right status code - "<condition>"
        When the user deletes a private bin with "<condition>"
        Then the response status should be "<responseCode>" for "invalidDeleteRequest"
        Examples:
            | condition    | responseCode |
            | invalidBinId | 400          |
            | emptyHeader  | 401          |
