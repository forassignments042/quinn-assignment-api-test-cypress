import { Then } from "cypress-cucumber-preprocessor/steps";
("use strict");
const deleteService = require("../../serviceObjectModel/DELETE");
let delReq = new deleteService();
(function () {
  Then("the response should contain the expected {string}", (error) => {
    expect(error).not.to.be.empty;
    delReq.shouldErrorMessageBe(error);
  });
  Then("the response should contain a success message", () => {
    delReq.successMessageShouldBeReturned();
  });
})();
