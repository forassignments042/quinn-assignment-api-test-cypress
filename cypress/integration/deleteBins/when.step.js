import { When } from "cypress-cucumber-preprocessor/steps";
("use strict");
const deleteService = require("../../serviceObjectModel/DELETE");
let delReq = new deleteService();
(function () {
  When("the user deletes a private bin", () => {
    delReq.deleteBin();
  });
  When("the user deletes a private bin with {string}", (condtion) => {
    expect(condtion).not.to.be.empty;
    delReq.sendReqWithCond(condtion);
  });
  When("the user deletes a second private bin", () => {
    delReq.deleteSecond();
  });
})();
