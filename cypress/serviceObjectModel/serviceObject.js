"use strict";
import routes from "../fixtures/routes.json";
import reqResp from "../fixtures/reqResp_data.json";
module.exports = class serviceObject {
  constructor(route) {
    this.endpoint = routes.baseEndpoint + route;
    this.header = {
      "X-Master-Key": reqResp.headers.validAPIKey,
    };
  }
  get _endpoint() {
    return this.endpoint;
  }
  get _header() {
    return this.header;
  }
  shouldReturnExpectedBody(response, expectedBody) {
    return cy.get("@" + response).should((response) => {
      expect(response.body).to.be.deep.equal(expectedBody);
    });
  }
  shouldResponseStatusCodeBe(response, responseCode) {
    return cy.get("@" + response).should((response) => {
      expect(response.status).to.eq(responseCode);
    });
  }
  get(route) {
    return cy.sendReq("GET", route, this._header).as("validGetRequest");
  }
  post(route, header, body) {
    return cy.sendReq("POST", route, header, body).as("validPostRequest");
  }
  put(route, header, body) {
    return cy.sendReq("PUT", route, header, body).as("validPutRequest");
  }
  delete(route) {
    return cy.sendReq("DELETE", route, this._header).as("validDeleteRequest");
  }
  invalidGet(route, header = this._header) {
    return cy.sendReq("GET", route, header).as("invalidGetRequest");
  }
  invalidPost(route, header = this._header, body = {}) {
    return cy.sendReq("POST", route, header, body).as("invalidPostRequest");
  }
  invalidPut(route, header = this._header, body) {
    return cy.sendReq("PUT", route, header, body).as("invalidPutRequest");
  }

  invalidDelete(route, header = this._header) {
    return cy.sendReq("DELETE", route, header).as("invalidDeleteRequest");
  }
};
