"use strict";

const serviceObject = require("./serviceObject");
import binData from "../fixtures/binData.json";
import reqResp from "../fixtures/reqResp_data.json";
import message from "../fixtures/messages.json";
import routes from "../fixtures/routes.json";
module.exports = class PUT extends serviceObject {
  constructor() {
    super(routes.GET_PUT_DELETE);
    this.binId = binData.binId_toUpdate;
    this.invalidBinId = binData.invalidBinId;
    this.notExistingBinId = binData.notExistingBinId;
    this.body = reqResp.bodies.body_forUpdate;
    this.route = this._endpoint;
    this.putHeader = {
      "X-Master-Key": this._header["X-Master-Key"],
      "Content-Type": "application/json",
    };
    this.headerWithWrongContentType = {
      "X-Master-Key": this._header["X-Master-Key"],
      "Content-Type": "something",
    };
    this.error = {
      invalidOrEmptyContentType: message.invalidOrEmptyContentType,
      binNotFound: message.put_binNotFound,
      invalidBinIdError: message.invalidBinId,
      blankBin: message.blankBin,
      emptyHeader: message.put_emptyHeader,
    };
  }
  get _binId() {
    return this.binId;
  }
  get _notExistingBinId() {
    return this.notExistingBinId;
  }
  get _body() {
    return this.body;
  }
  set _newRoute(id) {
    this.newroute = this._route + id;
  }
  get _route() {
    return this.route;
  }
  get _putHeader() {
    return this.putHeader;
  }
  get _headerWithWrongContentType() {
    return this.headerWithWrongContentType;
  }
  get _invalidOrEmptyContentType() {
    return this.error.invalidOrEmptyContentType;
  }
  get _binNotFound() {
    return this.error.binNotFound;
  }
  get _invalidBinId() {
    return this.invalidBinId;
  }
  get _blankBin() {
    return this.error.blankBin;
  }
  get _emptyHeader() {
    return this.error.emptyHeader;
  }
  get _invalidBinIdError() {
    return this.error.invalidBinIdError;
  }

  update_JsonBin() {
    this._newRoute = this._binId;
    return this.put(this.newroute, this._putHeader, this._body);
  }
  doesUpdatedBinContainsExpectedRecord() {
    return cy.get("@validPutRequest").should((response) => {
      expect(response.body.record).to.deep.equal(this._body);
    });
  }
  shouldErrorMessageBe(cond) {
    switch (cond) {
      case "invalidBinId":
        return this.shouldReturnExpectedBody(
          "invalidPutRequest",
          this._invalidBinIdError
        );
      case "binNotFound":
        return this.shouldReturnExpectedBody(
          "invalidPutRequest",
          this._binNotFound
        );
      case "invalidContentType":
        return this.shouldReturnExpectedBody(
          "invalidPutRequest",
          this._invalidOrEmptyContentType
        );
      case "blankBin":
        return this.shouldReturnExpectedBody(
          "invalidPutRequest",
          this._blankBin
        );
      case "emptyHeader":
        return this.shouldReturnExpectedBody(
          "invalidPutRequest",
          this._emptyHeader
        );
    }
  }
  sendReqWithCond(cond) {
    switch (cond) {
      case "invalidBinId":
        this._newRoute = this._invalidBinId;
        return this.invalidPut(this.newroute, this._putHeader, this._body);
      case "binNotFound":
        this._newRoute = this._notExistingBinId;
        return this.invalidPut(this.newroute, this._putHeader, this._body);
      case "invalidContentType":
        this._newRoute = this._binId;
        return this.invalidPut(
          this.newroute,
          this._headerWithWrongContentType,
          this._body
        );
      case "blankBin":
        this._newRoute = this._binId;
        return this.invalidPut(this.newroute, this._header, {});
      case "emptyHeader":
        this._newRoute = this._binId;
        return this.invalidPut(this.newroute, {}, this._body);
    }
  }
};
