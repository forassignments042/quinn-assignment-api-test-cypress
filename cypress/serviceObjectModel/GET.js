"use strict";

const serviceObject = require("./serviceObject");
import binData from "../fixtures/binData.json";
import reqResp from "../fixtures/reqResp_data.json";
import message from "../fixtures/messages.json";
module.exports = class GET extends serviceObject {
  constructor() {
    super("/b/");
    this.route = this._endpoint;
    this.binId = binData.binId_toRetrieve;
    this.invalidBinId = binData.invalidBinId;
    this.notExistingBinId = binData.notExistingBinId;
    this.header["X-BIN-META"] = false;
    this.invalidHeader = reqResp.headers.invalidAPIKey;
    this.expectedPrivateBinBody = reqResp.bodies.RetrievedBody;
    this.error = {
      invalidBinId: message.invalidBinId,
      binNotFound: message.binNotFound,
      invalidHeader: message.invalidHeader,
      emptyHeader: message.get_emptyHeader,
    };
  }
  get _errorBinNotFound() {
    return this.error.binNotFound;
  }
  get _errorEmptyHeader() {
    return this.error.emptyHeader;
  }
  get _errorInvalidHeader() {
    return this.error.invalidHeader;
  }
  get _errorInvalidBinId() {
    return this.error.invalidBinId;
  }
  get _header() {
    return this.header;
  }
  get _route() {
    return this.route;
  }
  get _binId() {
    return this.binId;
  }
  get _expectedPrivateBinBody() {
    return this.expectedPrivateBinBody;
  }
  get _invalidBinId() {
    return this.invalidBinId;
  }
  get _notExistingBinId() {
    return this.notExistingBinId;
  }
  get _invalidHeader() {
    return this.invalidHeader;
  }
  readPrivateBin() {
    return this.get(this._route + this._binId);
  }
  shouldErrorMessageBe(error) {
    switch (error) {
      case "invalidBinId":
        return this.shouldReturnExpectedBody(
          "invalidGetRequest",
          this._errorInvalidBinId
        );
      case "binNotFound":
        return this.shouldReturnExpectedBody(
          "invalidGetRequest",
          this._errorBinNotFound
        );
      case "invalidHeader":
        return this.shouldReturnExpectedBody(
          "invalidGetRequest",
          this._errorInvalidHeader
        );
      case "emptyHeader":
        return this.shouldReturnExpectedBody(
          "invalidGetRequest",
          this._errorEmptyHeader
        );
    }
  }
  expectedPrivateBinBodyShouldBeReturned(response) {
    return this.shouldReturnExpectedBody(
      response,
      this._expectedPrivateBinBody
    );
  }
  set binIdforInvalidRoute(id) {
    this.newInvalidRoute = this.route + id;
  }
  set binIdforValidRoute(id) {
    this.newValidRoute = this.route + id;
  }
  sendReqWithCond(cond) {
    switch (cond) {
      case "invalidBinId":
        this.binIdforInvalidRoute = this._invalidBinId;
        return this.invalidGet(this.newInvalidRoute, this._header);
      case "binNotFound":
        this.binIdforInvalidRoute = this._notExistingBinId;
        return this.invalidGet(this.newInvalidRoute, this._header);
      case "invalidHeader":
        this.binIdforValidRoute = this._binId;
        return this.invalidGet(this.newValidRoute, this._invalidHeader);
      case "emptyHeader":
        this.binIdforValidRoute = this._binId;
        return this.invalidGet(this.newValidRoute, {});
    }
  }
};
