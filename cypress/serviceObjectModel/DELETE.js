"use strict";

const serviceObject = require("./serviceObject");
import binData from "../fixtures/binData.json";
import message from "../fixtures/messages.json";
module.exports = class DELETE extends serviceObject {
  constructor() {
    super("/b/");
    this.route = this._endpoint;
    this.binId_toDelete_1 = binData.binIdtodelete_1;
    this.binId_toDelete_2 = binData.binIdtodelete_2;
    this.invalidBinId = binData.invalidBinId;
    this.successMessage = {
      metadata: {
        id: this.binId_toDelete_2,
        versionsDeleted: 0,
      },
      message: message.successMessage.message,
    };
    this.error = {
      InvalidBinId: message.invalidBinId,
      emptyHeader: message.emptyHeader,
    };
  }
  get _successMessage() {
    return this.successMessage;
  }
  get _errorInvalidBinId() {
    return this.error.InvalidBinId;
  }
  get _errorEmptyHeader() {
    return this.error.emptyHeader;
  }
  get _route() {
    return this.route;
  }
  get _binId_toDelete_2() {
    return this.binId_toDelete_2;
  }
  get _binId_toDelete_1() {
    return this.binId_toDelete_1;
  }
  get _invalidBinId() {
    return this.invalidBinId;
  }
  set invalidRoute(id) {
    this.newInvalidRoute = this.route + id;
  }
  set validRoute(id) {
    this.newValidRoute = this.route + id;
  }
  deleteBin() {
    return this.delete(this._route + this._binId_toDelete_1);
  }
  deleteSecond() {
    return this.delete(this._route + this._binId_toDelete_2);
  }
  successMessageShouldBeReturned() {
    return this.shouldReturnExpectedBody(
      "validDeleteRequest",
      this._successMessage
    );
  }
  sendReqWithCond(cond) {
    switch (cond) {
      case "invalidBinId":
        this.invalidRoute = this._invalidBinId;
        return this.invalidDelete(this.newInvalidRoute);
      case "emptyHeader":
        this.validRoute = this._binId_toDelete_1;
        return this.invalidDelete(this.newInvalidRoute, {});
    }
  }
  shouldErrorMessageBe(error) {
    switch (error) {
      case "invalidBinId":
        return this.shouldReturnExpectedBody(
          "invalidDeleteRequest",
          this._errorInvalidBinId
        );
      case "emptyHeader":
        return this.shouldReturnExpectedBody(
          "invalidDeleteRequest",
          this._errorEmptyHeader
        );
    }
  }
};
