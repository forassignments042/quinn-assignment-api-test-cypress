"use strict";

const serviceObject = require("./serviceObject");
import reqResp from "../fixtures/reqResp_data.json";
import message from "../fixtures/messages.json";
module.exports = class POST extends serviceObject {
  constructor() {
    super("/b");
    this.body = reqResp.bodies.body_forCreation;
    this.route = this._endpoint;
    this.postHeader = {
      "X-Master-Key": this._header["X-Master-Key"],
      "Content-Type": "application/json",
    };
    this.headerWithWrongContentType = {
      "X-Master-Key": this._header["X-Master-Key"],
      "Content-Type": "something",
    };
    this.error = {
      invalidOrEmptyContentType: message.invalidOrEmptyContentType,
      blankBin: message.blankBin,
      emptyHeader: message.emptyHeader,
    };
  }
  get _invalidOrEmptyContentType() {
    return this.error.invalidOrEmptyContentType;
  }
  get _blankBin() {
    return this.error.blankBin;
  }
  get _emptyHeader() {
    return this.error.emptyHeader;
  }
  get _headerWithWrongContentType() {
    return this.headerWithWrongContentType;
  }
  get _postHeader() {
    return this.postHeader;
  }
  get _body() {
    return this.body;
  }
  get _route() {
    return this.route;
  }
  add_JsonBin() {
    return this.post(this.route, this._postHeader, this._body);
  }
  doesCreatedBinContainsExpectedRecord() {
    return cy.get("@validPostRequest").should((response) => {
      expect(response.body.record).to.deep.equal(this._body);
    });
  }
  shouldErrorMessageBe(cond) {
    switch (cond) {
      case "invalidContentType":
        return this.shouldReturnExpectedBody(
          "invalidPostRequest",
          this._invalidOrEmptyContentType
        );
      case "blankBin":
        return this.shouldReturnExpectedBody(
          "invalidPostRequest",
          this._blankBin
        );
      case "emptyHeader":
        return this.shouldReturnExpectedBody(
          "invalidPostRequest",
          this._emptyHeader
        );
    }
  }
  sendReqWithCond(cond) {
    switch (cond) {
      case "invalidContentType":
        return this.invalidPost(
          this._route,
          this._headerWithWrongContentType,
          this._body
        );
      case "blankBin":
        return this.invalidPost(this._route, this._postHeader);
      case "emptyHeader":
        return this.invalidPost(this._route, {});
    }
  }
};
