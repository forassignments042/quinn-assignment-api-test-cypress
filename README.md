# quinn-assignment-api-test-cypress
 
# quinn-assignment-api-test-cypress
about the assignement :
the project structure :

fixtures : contain json file holding data about error/success messages and test data.

integration : contains the spec needed to run the tests [comon directory contains reusable steps among all tests - Features contains all feature files and step definitions).

serviceObjectModel : service classes and generic cypress methods .

plugin : index.js where plugins are connected.

reports: at the end of the test run , an html file will be issued display merged test results
this file will have assets that contain screenshots.

support : contain Testdata and ObjectData on demande builders, index.js/commandjs for cypress custom commands.

package.json : you can find here different run commands and devDependencies used to run this project .

install dependencies:
after cloning the repository locally , open the terminal in the project directory and enter :

npm install --save-dev

to test installation enter the following command:

npm run test

this will open Cypres UI launcher , where two spec files should be displayed
in order to run the tests :
2 ways are available :

locally :


commands/scripts:

npm run e2e:smoke

->will run one and only test tagged as smoke test

npm run e2e:regression

->will run all remaining tests

npm run run:all

->this will run all tests


reporting : mochawesome report will merge all reports into an Html file under cypress/reports/html/index.html
the report will contain screenshots of failed tests and the attempts.


Cypress Dashboard: realtime reporting , an invitation should've been sent to the reviewer



using yml pipeline :

using gitlab pipeline by manual trigger or Push/PR on GITLAB: an invitation should've been sent to the reviewer

reports are stored as artifacts as long as videos and independent screenshots.
the run is also recorded in cypress dashboard
